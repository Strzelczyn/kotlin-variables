fun main(args: Array<String>){
    var typeVarString: String = "sd"
    var typeVarChar: Char = 'd'
    var typeVarAuto = 'z' //type char
    var typeVarAuto1 = "sa" //type string
    var typeVarDouble: Double = 3.14
    var typeVarFloat: Float = 0.5F
    var typeVarAuto2 = 3.22 //double
    var typeVarInt: Int = 3
    var typeVarLong: Long = 5L
    var typeVarByte: Byte = 5
    var typeVarShort: Short = 52
    var typeVarAuto3 = 3 //int
}